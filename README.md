# Wobiz Login Back

## Technologies :
- NodeJS
- Docker
- Docker Compose

## Libraries :
- Express

## How to run:
- `npm install`
- `npm run dev`

## Local port:
- 3000

## How to run with Docker Compose:
- docker-compose up -d --build

## Docker port:
- 3000