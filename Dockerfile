# Build stage
FROM node:jessie as build-stage
WORKDIR /app
ADD package*.json ./
RUN npm install
ADD . .
EXPOSE 3000
CMD [ "npm", "run", "start" ]