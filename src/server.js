const { response } = require('express');
const express = require('express');
const config = require("./config");

const app = express();

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
});
app.use(express.json());

app.post('/login', function(req, res) {
    let  response = {};
    const user = {
        username: "admin@gmail.com",
        password: "adminpass"
    };

    if (req.body.username === user.username && req.body.password === user.password) {
        response.code = 200;
        response.token = "ba7c2cf6c55e3e382f2f48231aafc6b8725d723b";
        response.expires = 1567619449;
        response.user_id = 21432;
        res.status(200).send(response);
        return;
    } 
    
    if (req.body.username !== user.username) {
        response.success = false;
        response.code = 108;
        response.message = "Wrong username";
        res.status(401).send(response);
        return;
    }

    if (req.body.password !== user.password) {
        response.success = false;
        response.code = 106;
        response.message = "Wrong password for user";
        res.status(401).send(response);
        return;
    }
})

app.listen(config.PORT, () => {
    console.log(`Server on port ${config.PORT}`);
});